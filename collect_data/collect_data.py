import pandas as pd
from datetime import datetime
from os import path
from high_scores import high_scores

__bullet = 0
__enemy = 0
__coin = 0
__movement = 0

db_connection = high_scores.connect_to_db()
high_scores.activity_log_create_table(db_connection)

def reset_log():
    global __bullet
    global __enemy
    global __coin
    global __movement
    __bullet = 0
    __enemy = 0
    __coin = 0
    __movement = 0

def push(player_name , x, y):
    global __bullet, __enemy, __coin
    __shots_on_target = __enemy
    __shots_off_target = __bullet - __enemy
    __ratio_shots_on_target = 0 if __enemy == 0  else __enemy / __bullet
    print('-----------------------------------------------')
    print('x : ' + str(x))
    print('y : ' + str(y))
    print('shots : ' + str(__bullet))
    print('shots_on_target : ' + str(__shots_on_target))
    print('shots_off_target : ' + str(__shots_off_target))
    print('ratio shots_on_target : ' + str(__ratio_shots_on_target))
    print('movement : ' + str(__movement))
    print('coin : ' + str(__coin))
    print('-----------------------------------------------')
    now = datetime.now()
    model = {
        'timestamp': float(datetime.timestamp(now)),
        'name': player_name,
        'x': x,
        'y': y,
        'shots': __bullet,
        'on_target': __shots_on_target,
        'off_target': __shots_off_target,
        'shots_ratio': __ratio_shots_on_target,
        'movement': __movement,
        'coin': __coin
    }
    insert_csv(model)

def fire_bullet():
    global __bullet
    __bullet += 1

def fire_on_target():
    global __enemy
    __enemy += 1
    
def collect_coin():
    global __coin
    __coin += 1

def movement():
    global __movement
    __movement += 1

def get_summarys(player_name):
    global __bullet, __enemy, __coin
    __shots_on_target = __enemy
    __shots_off_target = __bullet - __enemy
    __ratio_shots_on_target = 0 if __enemy == 0  else __enemy / __bullet
    return {
        'name': player_name,
        'shots': __bullet,
        'on_target': __shots_on_target,
        'off_target': __shots_off_target,
        'shots_ratio': __ratio_shots_on_target,
        'movement': __movement,
        'coin': __coin
    }

def insert_csv(model):
    csv_file = "activity_log.csv"
    colums = ['timestamp', 'name', 'x', 'y', 'shots', 'on_target', 'off_target', 'shots_ratio', 'movement', 'coin']
    df = pd.DataFrame([model], columns=colums)


    if path.exists(csv_file):
        df.to_csv(csv_file, mode='a', header=False)
    else:
        df.to_csv(csv_file, mode='a')
    
    



