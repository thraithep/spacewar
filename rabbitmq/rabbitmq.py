import pika
import json

def create_connection(url, port, username, password, queue_name):
    credentials = pika.PlainCredentials(username, password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        url, port, '/', credentials))
    channel = connection.channel()
    return channel

def publish(channel, queue_name, obj):
    message = json.dumps(obj)
    channel.basic_publish(exchange='', routing_key=queue_name, body=message)

def consume(channel, queue_name, callback):
    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

def close_connection(connection):
    connection.close()
