import pika, sys, os
import rabbitmq 


url = '13.213.37.244'
port = 5672
username = 'worker'
password = 'Nio@1234'
queue_name = 'spacewar'



def main():
    channel = rabbitmq.create_connection(url, port, username, password, queue_name)
    channel.queue_declare(queue = queue_name)

    def callback(ch, method, properties, body):
        publisher_other_queue('Hardcore Killer')

    rabbitmq.consume(channel, queue_name)
    # credentials = pika.PlainCredentials(username, password)
    # connection = pika.BlockingConnection(pika.ConnectionParameters(
    #     url, port, '/', credentials))

    # channel = connection.channel()

    # channel.queue_declare(queue=queue_name)

    # def callback(ch, method, properties, body):
    #     print(" [x] Received %r" % body)
    #     publisher_other_queue()

    # channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

    # print(' [*] Waiting for messages. To exit press CTRL+C')
    # channel.start_consuming()

def publisher_other_queue(obj):
    credentials = pika.PlainCredentials(username, password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        url, port, '/', credentials))

    channel = connection.channel()

    channel.queue_declare(queue='return_spacewar')
    channel.basic_publish(exchange='', routing_key='return_spacewar', body='Hello World!')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            
            os._exit(0)