import pandas as pd
from datetime import datetime
import time
from os import path
import random

csv_file = 'activity_log.csv'

class_type = ['Hardcore achiever', 'Hardcore killer', 'Casual achiever', 'Casual killer']

for i in range(500):
    print(f'player{i}')
    enemy = 0
    bullet = 0
    coin = 0
    movement = 0
    

    for j in range(50):
        type_player = i % 4
        new_game = random.randint(0,5) 
        now = datetime.now()
        timestamp = float(datetime.timestamp(now))
        x = random.randint(0, 736)
        y = random.randint(0, 536)
        if new_game > 4 and type_player != 0 :
            enemy = 0
            bullet = 0
            coin = 0
            movement = 0
        else :
            enemy += random.randint(0, 4)
            bullet = enemy + random.randint(0, 10)
            coin += random.randint(0, type_player + 1)
            movement += enemy + coin + random.randint(1, type_player + 2)

        on_target = enemy
        off_target = bullet - enemy
        ratio = 0 if bullet == 0 else enemy / bullet
        
        model = {
            'timestamp': timestamp,
            'name': f'player{i+1}',
            'x': x,
            'y': y,
            'shots': bullet,
            'on_target': on_target,
            'off_target': off_target,
            'movement': movement,
            'coin': coin,
        }

        colums = ['timestamp', 'name', 'x', 'y', 'shots', 'on_target', 'off_target', 'movement', 'coin']
        df = pd.DataFrame([model], columns=colums)


        if path.exists(csv_file):
            df.to_csv(csv_file, mode='a', header=False)
        else:
            df.to_csv(csv_file, mode='a')

        time.sleep(0.1)

